FROM ubuntu:16.04

MAINTAINER Alexander Kusakin <alexander.a.kusakin@gmail.com>

ENV NPS_VERSION 1.12.34.2
ENV NGINX_VERSION 1.13.2
ENV NAXSI_VERSION 0.55.3

ENV OPENSSL_VERSION 1.1.0f
ENV ZLIB_VERSION 1.2.11
ENV PCRE_VERSION 8.40

RUN apt-get update
RUN apt-get install -y --no-install-recommends software-properties-common
RUN add-apt-repository -s ppa:nginx/stable
RUN add-apt-repository ppa:certbot/certbot
RUN apt-get update
RUN apt-get build-dep --no-install-recommends -y nginx-common
RUN apt-get install --no-install-recommends wget python-certbot-nginx unzip -y
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

RUN mkdir /tmp/custom-nginx/
WORKDIR /tmp/custom-nginx/

RUN wget --no-check-certificate https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz
RUN wget --no-check-certificate http://zlib.net/zlib-${ZLIB_VERSION}.tar.gz
RUN wget --no-check-certificate ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-${PCRE_VERSION}.tar.bz2
RUN wget --no-check-certificate https://github.com/nbs-system/naxsi/archive/${NAXSI_VERSION}.zip
RUN wget --no-check-certificate http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz
RUN wget --no-check-certificate https://github.com/pagespeed/ngx_pagespeed/archive/v${NPS_VERSION}-stable.zip
RUN unzip v${NPS_VERSION}-stable.zip && rm v${NPS_VERSION}-stable.zip

WORKDIR /tmp/custom-nginx/ngx_pagespeed-${NPS_VERSION}-stable/

RUN wget --no-check-certificate https://dl.google.com/dl/page-speed/psol/${NPS_VERSION}-x64.tar.gz
RUN tar xzvf ${NPS_VERSION}-x64.tar.gz && rm ${NPS_VERSION}-x64.tar.gz

WORKDIR /tmp/custom-nginx/

RUN tar xvf openssl-${OPENSSL_VERSION}.tar.gz && rm openssl-${OPENSSL_VERSION}.tar.gz
RUN tar xvf zlib-${ZLIB_VERSION}.tar.gz && rm zlib-${ZLIB_VERSION}.tar.gz
RUN tar xvf pcre-${PCRE_VERSION}.tar.bz2 && rm pcre-${PCRE_VERSION}.tar.bz2
RUN unzip ${NAXSI_VERSION}.zip && rm ${NAXSI_VERSION}.zip
RUN tar xvzf nginx-${NGINX_VERSION}.tar.gz && rm nginx-${NGINX_VERSION}.tar.gz

WORKDIR /tmp/custom-nginx/nginx-${NGINX_VERSION}/

RUN ./configure \
        --prefix=/usr/share/nginx \
        --with-cc-opt='-m64 -g -O3 -flto -pipe -fwhole-program -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -Wno-sign-compare' \
        --with-ld-opt='-m64 -g -O3 -flto -pipe -fwhole-program -lrt -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security -D_FORTIFY_SOURCE=2 -Wall -Wl,-z,relro,-Bsymbolic-functions' \
        --add-module=../ngx_pagespeed-${NPS_VERSION}-stable ${PS_NGX_EXTRA_FLAGS} \
        --add-module=../naxsi-${NAXSI_VERSION}/naxsi_src/ \
        --with-http_gzip_static_module \
        --with-http_realip_module \
        --with-http_secure_link_module \
        --with-http_ssl_module \
        --with-http_v2_module \
        --with-openssl=../openssl-${OPENSSL_VERSION} \
        --with-pcre=../pcre-${PCRE_VERSION} \
        --with-pcre-jit \
        --with-stream \
        --with-stream_ssl_module \
        --with-threads \
        --with-zlib=../zlib-${ZLIB_VERSION} \
        --without-mail_imap_module \
        --without-mail_pop3_module \
        --without-mail_smtp_module \
        --user=www-data \
        --group=www-data \
        --conf-path=/etc/nginx/nginx.conf \
        --http-log-path=/var/log/nginx/access.log \
        --error-log-path=/var/log/nginx/error.log \
        --lock-path=/var/lock/nginx.lock \
        --pid-path=/run/nginx.pid
RUN make -j`nproc`
RUN make install

WORKDIR /tmp/custom-nginx/

RUN mkdir -p /etc/nginx/
RUN mkdir -p /var/log/nginx/
RUN cp naxsi-$NAXSI_VERSION/naxsi_config/naxsi_core.rules /etc/nginx/naxsi_core.rules
RUN rm -rf /tmp/custom-nginx/

WORKDIR /

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80 443

VOLUME /etc/nginx /var/www

CMD ["/usr/share/nginx/sbin/nginx", "-g", "daemon off;"]
